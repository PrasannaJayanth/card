<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cards</title>
    <link rel="stylesheet" href="./assests/css/bootstrap.min.css">
    <link rel="stylesheet" href="./card.css">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-image">
                        <img src="./nature.avif" class="card-img-top">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">The Beauty of Nature</h5>
                        <p class="card-text">"Nature" refers to the phenomena of the physical world, and also to life in general. It ranges in scale from the subatomic to the cosmic. The term "nature" may refer to living plants and animals, geological processes, weather, and physics, such as matter and energy.
                        </p>
                        <a class="card-text" href="#"><small class="text-muted">Learn more</small></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-image1">
                        <img src="./nature.avif" class="card-img-top">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">The Beauty of Nature</h5>
                        <p class="card-text">"Nature" refers to the phenomena of the physical world, and also to life in general. It ranges in scale from the subatomic to the cosmic. The term "nature" may refer to living plants and animals, geological processes, weather, and physics, such as matter and energy.</p>
                        <p class="card-text"><small class="text-muted">Learn more</small></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card1">
                    <img src="./nature.avif" class="card-img-top">
                    <div class="content">
                        <h2>The Beauty of Nature</h2>
                        <p>"Nature" refers to the phenomena of the physical world, and also to life in general. It ranges in scale from the subatomic to the cosmic. The term "nature" may refer to living plants and animals, geological processes, weather, and physics, such as matter and energy.</p>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>




    </div>



    <script src="./assests/js/bootstrap.min.js"></script>
</body>

</html>